#include <SPI.h>
#include <SD.h>
#include <Wire.h>
#include <RTClib.h>
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <OneWire.h>


//card
const int chipSelect = 4;
Sd2Card card;


RTC_DS1307 rtc;
#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

// Declaration for SSD1306 display connected using software SPI (default case):
#define OLED_MOSI   9
#define OLED_CLK   10
#define OLED_DC    11
#define OLED_CS    12
#define OLED_RESET 13
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT,
  OLED_MOSI, OLED_CLK, OLED_DC, OLED_RESET, OLED_CS);

File myFile;
File doorConditionsFile;
File temperatureFile;


 char *doorFileName="door.txt";
 char *temperatueFileName ="temp.txt";
bool firstwrite = false;
bool sd_working;

const int triggerPin = 2; //ultraäänimoduulin triggeröinti
const int echoPin = 3; //ultraäänimoduulin echo
const int ledPin = 4; //ledin pinni

OneWire  ds(6);  // on pin 10 (a 4.7K resistor is necessary)

int distance;
long sensor_time;

int temp; //Viimeisin mitattu lämpötila
 
bool door_open; //onko ovi auki vai kiinni, 1 auki 0 kiinni

float temperature;
float newTemperature;


void setup() {
  // Open serial communications and wait for port to open:
  Serial.begin(9600);

    //digital pin 2 output, joka triggeröi ultraäänimoduulin
  pinMode (triggerPin, OUTPUT);
  
  //digital pin 3 input. Ultraääni moduuli lähettää echon tähän
  pinMode(echoPin, INPUT);
    
 if (!card.init(SPI_HALF_SPEED, chipSelect)) {
    sd_working = false;
    while (1);
  } else {
    sd_working = true;
  }


  #ifndef ESP8266
  while (!Serial); // wait for serial port to connect. Needed for native USB
#endif

  if (! rtc.begin()) {
    Serial.println("Couldn't find RTC");
    Serial.flush();
    while (1) delay(10);
  }

  if (! rtc.isrunning()) {
    Serial.println("RTC is NOT running, let's set the time!");
    // When time needs to be set on a new device, or after a power loss, the
    // following line sets the RTC to the date & time this sketch was compiled
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    // This line sets the RTC with an explicit date & time, for example to set
    // January 21, 2014 at 3am you would call:
     //rtc.adjust(DateTime(2022, 3, 23, 17, 52, 0));
  }

  // When time needs to be re-set on a previously configured device, the
  // following line sets the RTC to the date & time this sketch was compiled
  // rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  // This line sets the RTC with an explicit date & time, for example to set
  // January 21, 2014 at 3am you would call:
   //rtc.adjust(DateTime(2022, 4, 27, 23, 5, 0));

  Serial.begin(9600);
display.begin(SSD1306_SWITCHCAPVCC);
display.display();


  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }

 Serial.print("Initializing SD card...");

  if (!SD.begin(4)) {
    Serial.println("initialization failed!");
    while (1);
  }
  Serial.println("initialization done.");
  
  // re-open the file for reading:
  /*myFile = SD.open("test.txt");
  if (myFile) {
    Serial.println("test.txt:");

    // read from the file until there's nothing else in it:
    while (myFile.available()) {
      Serial.write(myFile.read());
    }
    // close the file:
    myFile.close();
  } else {
    // if the file didn't open, print an error:
    Serial.println("error opening test.txt");
  }*/


  
  Serial.print("Arduino started!");

}


void loop() {

  TemperatureDetector();
  MotionDetector ();

     DateTime now = rtc.now();
display.clearDisplay();
display.setTextSize(1.5); 
display.setTextColor(SSD1306_WHITE);
display.setCursor(0, 0);

   char buf5[] = "DD MMM YYYY    hh:mm";
display.println(now.toString(buf5));

//display.println("");


display.display();

   delay(1000);
  // nothing happens after setup
}

void WriteDoorInfo(bool doorOpen)
{

    if (SD.exists(doorFileName)) {
        Serial.println("door.txt exists.");
                doorConditionsFile = SD.open(doorFileName, FILE_WRITE);

    } else {
        doorConditionsFile = SD.open(doorFileName, FILE_WRITE);
        //myFile.print("date , door condition");
       doorConditionsFile.print("date ; door condition");

        Serial.println("door doesn't exist.");
              DateTime now = rtc.now();
      char date[] = "DD.MM.YYYY hh:mm";
         if(doorOpen)
      {
          doorConditionsFile.println("");
          doorConditionsFile.print(now.toString(date));
          doorConditionsFile.print(" ; ");
          doorConditionsFile.print("door open");
       }
       else
       {
          doorConditionsFile.println("");
          doorConditionsFile.print(now.toString(date));
          doorConditionsFile.print(" ; ");
          doorConditionsFile.print("door close");
        }

      }
      //myFile = SD.open(doorFileName, FILE_WRITE);

      // if the file opened okay, write to it:
    if (doorConditionsFile) 
    {
      Serial.print("Writing to door.txt...");

    
      DateTime now = rtc.now();
      char date[] = "DD.MM.YYYY hh:mm";
      if(doorOpen)
      {
          doorConditionsFile.println("");
          doorConditionsFile.print(now.toString(date));
          doorConditionsFile.print(" ; ");
          doorConditionsFile.print("door open");
                        Serial.println("door open.");

       }
       else
       {
          doorConditionsFile.println("");
          doorConditionsFile.print(now.toString(date));
          doorConditionsFile.print(" ; ");
          doorConditionsFile.print("door close");
              Serial.println("door close.");

        }
    // close the file:
    doorConditionsFile.close();
    Serial.println("done.");
    } 
    else
    {
      // if the file didn't open, print an error:
      Serial.println("error opening test.txt");
    }

  // re-open the file for reading:
    /*doorConditionsFile = SD.open(doorFileName);
    if (doorConditionsFile) 
    {
        Serial.println("door.txt:");
        // read from the file until there's nothing else in it:
       while (doorConditionsFile.available()) 
       {
          Serial.write(doorConditionsFile.read());
        }
        // close the file:
        doorConditionsFile.close();
      } 
      else 
      {
    // if the file didn't open, print an error:
        Serial.println("error opening test.txt");
      }*/
  //SD.remove(doorFileName);

}

void WriteTemperatureInfo(float temperature)
{

    if (SD.exists(temperatueFileName)) {
        Serial.println("door exists.");
                temperatureFile = SD.open(temperatueFileName, FILE_WRITE);

    } else {
        temperatureFile = SD.open(temperatueFileName, FILE_WRITE);
        //myFile.print("date , door condition");
       temperatureFile.print("date ; temperature(celsius)");

        Serial.println("door doesn't exist.");
         DateTime now = rtc.now();
      char date[] = "DD.MM.YYYY hh:mm";

          temperatureFile.println("");
          temperatureFile.print(now.toString(date));
          temperatureFile.print(" ; ");
          temperatureFile.print(GetFLoatFromString(temperature));

      }

      // if the file opened okay, write to it:
    if (temperatureFile) 
    {
      Serial.print("Writing to test.txt...");

    
      DateTime now = rtc.now();
      char date[] = "DD.MM.YYYY hh:mm";

          temperatureFile.println("");
          temperatureFile.print(now.toString(date));
          temperatureFile.print(" ; ");
          temperatureFile.print(GetFLoatFromString(temperature));
      
    // close the file:
    temperatureFile.close();
    Serial.println("done.");
    } 
    else
    {
      // if the file didn't open, print an error:
      Serial.println("error opening test.txt");
    }

  // re-open the file for reading:
    /*temperatureFile = SD.open(temperatueFileName);
    if (temperatureFile) 
    {
        Serial.println("door.txt:");
        // read from the file until there's nothing else in it:
       while (temperatureFile.available()) 
       {
          Serial.write(temperatureFile.read());
        }
        // close the file:
        temperatureFile.close();
      } 
      else 
      {
    // if the file didn't open, print an error:
        Serial.println("error opening test.txt");
      }*/
  //SD.remove(doorFileName);

}

String GetFLoatFromString(float num){
  String floatString = String(num);
  floatString.replace('.', ',');
  Serial.println(floatString);
  return floatString;
}


int MeasureTemp(int door_open) {
  int temp = 0;
  
  Serial.print(" Requesting temperatures...");

  if (door_open == 0) {
  temp=0; //ovi on juuri suljettu, mitataan lämpötila temp muuttujaan
  Serial.print("Temperature after door was closed: ");
  }
  else 
  {
  temp=1; //jos ovi on auki, mitataan lämpötila temp2 muuttujaan
  Serial.print("Temperature when door was opened: ");
  }
  return(temp);
}
void MotionDetector () {
//etäisyysmittarin osuus
//mittaa etäisyyden sekunnin välein

  digitalWrite(triggerPin, LOW); //resetoidaan triggeröinti pin
  delayMicroseconds(2);
  digitalWrite(triggerPin, HIGH); //TriggerPin pulssi HIGH tilaan 10 microsekunniksi
  delayMicroseconds(10);
  digitalWrite(triggerPin, LOW); //Triggerpin pulssi LOW tilaan

  //Luetaan echoPin arvo, se palauttaa äänen liikkuman ajan mikrosekunneissa
  sensor_time = pulseIn(echoPin, HIGH);


  distance = sensor_time * 0.034 / 2;
  Serial.print(distance);

  if (distance > 100 && door_open==0) //Jos etäisyys on yli 100 cm ja ovi on aikaisemmin ollut suljettuna
  {
    digitalWrite(ledPin, HIGH);
  door_open=1;
  /*temp = MeasureTemp(door_open); 
  Serial.println(temp);*/
  WriteDoorInfo(door_open);
  
  }
  else if (distance < 100 && door_open==1)//Jos distance on alle 100cm ja ovi on aikaisemmin ollut auki
  {
    digitalWrite(ledPin, LOW);
  door_open=0;
  /*temp = MeasureTemp(door_open);
  Serial.println(temp);*/
    WriteDoorInfo(door_open);

  }
}

void TemperatureDetector (){
  getTemperature();
      //Serial.println(newTemperature);

  float tempMinus = temperature - newTemperature;
  if (tempMinus < 0){
    tempMinus *= -1;
  }

  if (tempMinus > 2){
    temperature = newTemperature;
    Serial.println("Temperature changed");
    Serial.println(temperature);
    WriteTemperatureInfo(temperature);

    
  }
  
}


void getTemperature(){
  byte i;
  byte present = 0;
  byte type_s;
  byte data[12];
  byte addr[8];
  float celsius, fahrenheit;
    if ( !ds.search(addr)) {
    ds.reset_search();
    delay(250);
    return;
  }
    for( i = 0; i < 8; i++) {
  }
  ds.reset();
  ds.select(addr);
  ds.write(0x44, 1);        
  delay(1000);

  present = ds.reset();
  ds.select(addr);    
  ds.write(0xBE); 

  for ( i = 0; i < 9; i++) {
    data[i] = ds.read();
  }
  int16_t raw = (data[1] << 8) | data[0];
  if (type_s) {
    raw = raw << 3; 
    if (data[7] == 0x10) {
      
      raw = (raw & 0xFFF0) + 12 - data[6];
    }
  } else {
    byte cfg = (data[4] & 0x60);
    if (cfg == 0x00) raw = raw & ~7;
    else if (cfg == 0x20) raw = raw & ~3;
    else if (cfg == 0x40) raw = raw & ~1;
  }
  celsius = (float)raw / 16.0;
  fahrenheit = celsius * 1.8 + 32.0;

  newTemperature = celsius;
}
