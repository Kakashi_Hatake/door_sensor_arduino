#include "pitches.h"

const int doorOpenPin = 2; // the number of the door open pin
const int ledPin =  13; // the number of the LED pin
bool tonePlayed = false; // Boolean variable for if tone played has been checked
int doorState = 0; // Variable for door state

void setup() {
  pinMode(doorOpenPin, INPUT);
  pinMode(ledPin, OUTPUT);
}

void loop() {
  doorState = digitalRead(doorOpenPin);
  if (doorState == HIGH) {
    digitalWrite(ledPin, LOW);
    playTone();
  } else {
    digitalWrite(ledPin, HIGH);
    tonePlayed = false;
  }
}

void playTone() {
  if (tonePlayed) {
    return;
  }
  // Play tones
  tone(8, NOTE_C4, noteDuration(8));
  // To distinguish the notes, set a minimum time between them.
  // the note's duration + 30% seems to work well
  delay(noteDuration(8) * 1.3);
  tone(8, NOTE_C4, noteDuration(8));
  delay(noteDuration(8) * 1.3);
  tone(8, NOTE_C5, noteDuration(2));
  delay(noteDuration(2) * 1.3);
  // Set tonePlayed variable to true so the tone is not constantly played while the door is open
  tonePlayed = true;
}

/**
 * Note duration.
 * 
 * To calculate the note duration, take one second divided by the note type.
 * e.g. quarter note = 1000 / 4, eighth note = 1000 / 8, etc.
 */
int noteDuration(int duration) {
  return 1000 / duration;
}
