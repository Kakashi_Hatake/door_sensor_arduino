const int triggerPin = 2; //ultraäänimoduulin triggeröinti
const int echoPin = 3; //ultraäänimoduulin echo
const int ledPin = 4; //ledin pinni

int etaisyys;
long kesto;

int lampo1; //15 minuutin välein mitattava lämpötila
int lampo2; //mitattava lämpötila, kun ovi on suljettuna
int oviauki; //onko ovi auki vai kiinni, 1 auki 0 kiinni

//lämpötilaosio alkaa

#include <OneWire.h>
#include <DallasTemperature.h> //lämpötilamittarin kirjastot

// Data wire is plugged into pin 6 on the Arduino
#define ONE_WIRE_BUS 6
 
// Setup a oneWire instance to communicate with any OneWire devices 
// (not just Maxim/Dallas temperature ICs)
OneWire oneWire(ONE_WIRE_BUS);
 
// Pass our oneWire reference to Dallas Temperature.
DallasTemperature sensors(&oneWire);

#define LED 12 //

volatile byte count=0;
void setup()
{
Serial.begin(9600);
Serial.print("Timer int test \n");
pinMode(LED, OUTPUT);

//määritetään kellon rekisteriarvot

noInterrupts(); // disable all interrupts
TCCR1A = 0;
TCCR1B = 0;
TCNT1 = 0;
OCR1A = 62500; // compare match register 16MHz/256/2Hz
TCCR1B |= (1 << WGM12); // CTC mode
TCCR1B |= (1 << CS12); // 256 prescaler
TIMSK1 |= (1 << OCIE1A); // enable timer compare interrupt
interrupts(); // enable all interrupts

sensors.begin();

  //digital pin 2 output, joka triggeröi ultraäänimoduulin
  pinMode (triggerPin, OUTPUT);
  
  //digital pin 3 input. Ultraääni moduuli lähettää echon tähän
  pinMode(echoPin, INPUT);

  pinMode(ledPin, OUTPUT);
}
void loop()
{
}
ISR(TIMER1_COMPA_vect) // timer compare interrupt service routine
{
//etäisyysmittarin osuus
//mittaa etäisyyden sekunnin välein

  digitalWrite(triggerPin, LOW); //resetoidaan triggeröinti pin
  delayMicroseconds(2);
  digitalWrite(triggerPin, HIGH); //TriggerPin pulssi HIGH tilaan 10 microsekunniksi
  delayMicroseconds(10);
  digitalWrite(triggerPin, LOW); //Triggerpin pulssi LOW tilaan

  //Luetaan echoPin arvo, se palauttaa äänen liikkuman ajan mikrosekunneissa
  kesto = pulseIn(echoPin, HIGH);

  // Seuraavaksi lasketaan etäisyys
  // Äänen nopeus v=340 m/s
  // v=0,034 cm/us
  // Aika = etäisyys / nopeus
  // t = s/v = 10 / 0,034 = 294 us
  // Jaetaan kahdella koska ääni kulkee molempiin suuntiin eli s = t * 0,034 / 2
  etaisyys = kesto * 0.034 / 2;

  if (etaisyys > 100) //Jos etäisyys on yli 100 cm, ledi menee päälle (ovi on auki)
  {
    digitalWrite(ledPin, HIGH);
	oviauki=1;
  }
  else //Jos etaisyys on alle 100cm, ledi sammuu (ovi on kiinni)
  {
    digitalWrite(ledPin, LOW);
	oviauki=0;
  }
  //Printtaukset
  Serial.print("\nEtaisyys: ");
  Serial.println(etaisyys);
  //Serial.print(0); //plotterille alaraja.
  Serial.print(" ");
  //Serial.print(300); //plotterille yläraja
  Serial.print(" ");
  //print ultraääni moduulin antama arvo

//lämpömittariosio alkaa
//mittaa lämpötilan 10 sekunnin välein
count++;
if (count == 10) {
  count = 0;
// call sensors.requestTemperatures() to issue a global temperature
  // request to all devices on the bus
  Serial.print(" Requesting temperatures...");
  //sensors.requestTemperatures(); // Send the command to get temperatures
  //Serial.println("DONE");

if (oviauki == 0) {
lampo1=sensors.getTempCByIndex(0); //jos ovi on kiinni, mitataan lämpötila lampo1 muuttujaan
Serial.print("Temperature1 is: ");
  Serial.print(lampo1); 
}
else 
{
lampo2=sensors.getTempCByIndex(0); //jos ovi on auki, mitataan lämpötila lampo2 muuttujaan
Serial.print("Temperature2 is: ");
  Serial.print(lampo2); 
}
  //Serial.print("Temperature is: ");
  //Serial.print(sensors.getTempCByIndex(0)); // Why "byIndex"? 
    // You can have more than one IC on the same bus. 
    // 0 refers to the first IC on the wire
}
/*Serial.print(count);
Serial.print("s: ");
Serial.print("\n");
*/
digitalWrite(LED, digitalRead(LED) ^ 1); //xor
}
