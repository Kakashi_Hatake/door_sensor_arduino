/*
 Ultrasonic Sensor HC-SR04 
 Koodi perustuu https://howtomechatronics.com/tutorials/arduino/ultrasonic-sensor-hc-sr04/
 by Dejan Nedelkovski, Ultrasonic Sensor HC-SR04 and Arduino Tutorial
  www.HowToMechatronics.com

*/
const int triggerPin = 2; //ultraäänimoduulin triggeröinti
const int echoPin = 3; //ultraäänimoduulin echo
const int ledPin = 4; //ledin pinni

int etaisyys;
long kesto;

void setup() {
  //sarjayhteys
  Serial.begin(9600);



  //digital pin 2 output, joka triggeröi ultraäänimoduulin
  pinMode (triggerPin, OUTPUT);
  
  //digital pin 3 input. Ultraääni moduuli lähettää echon tähän
  pinMode(echoPin, INPUT);

  pinMode(ledPin, OUTPUT);
}

void loop() {
  digitalWrite(triggerPin, LOW); //resetoidaan triggeröinti pin
  delayMicroseconds(2);
  digitalWrite(triggerPin, HIGH); //TriggerPin pulssi HIGH tilaan 10 microsekunniksi
  delayMicroseconds(10);
  digitalWrite(triggerPin, LOW); //Triggerpin pulssi LOW tilaan

  //Luetaan echoPin arvo, se palauttaa äänen liikkuman ajan mikrosekunneissa
  kesto = pulseIn(echoPin, HIGH);

  // Seuraavaksi lasketaan etäisyys
  // Äänen nopeus v=340 m/s
  // v=0,034 cm/us
  // Aika = etäisyys / nopeus
  // t = s/v = 10 / 0,034 = 294 us
  // Jaetaan kahdella koska ääni kulkee molempiin suuntiin eli s = t * 0,034 / 2
  etaisyys = kesto * 0.034 / 2;

  if (etaisyys > 100) //Jos etäisyys on yli 100 cm, ledi menee päälle (ovi on auki)
  {
    digitalWrite(ledPin, HIGH);
  }
  else //Jos etaisyys on alle 100cm, ledi sammuu (ovi on kiinni)
  {
    digitalWrite(ledPin, LOW);
  }
  //Printtaukset
  Serial.print("Etaisyys: ");
  Serial.println(etaisyys);
  Serial.print(0); //plotterille alaraja.
  Serial.print(" ");
  Serial.print(300); //plotterille yläraja
  Serial.print(" ");
  //print ultraääni moduulin antama arvo


}
