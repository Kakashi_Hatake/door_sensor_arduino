const int triggerPin = 2; //ultraäänimoduulin triggeröinti
const int echoPin = 3; //ultraäänimoduulin echo
const int ledPin = 4; //ledin pinni

int distance;
long sensor_time;

int temp; //Viimeisin mitattu lämpötila
 
bool door_open; //onko ovi auki vai kiinni, 1 auki 0 kiinni

//lämpötilaosio alkaa

#include <OneWire.h>
#include <DallasTemperature.h> //lämpötilamittarin kirjastot

// Data wire is plugged into pin 6 on the Arduino
#define ONE_WIRE_BUS 6
 
// Setup a oneWire instance to communicate with any OneWire devices 
// (not just Maxim/Dallas temperature ICs)
OneWire oneWire(ONE_WIRE_BUS);
 
// Pass our oneWire reference to Dallas Temperature.
DallasTemperature sensors(&oneWire);

#define LED 12 //

void setup()
{
Serial.begin(9600);
pinMode(LED, OUTPUT);


sensors.begin();

  //digital pin 2 output, joka triggeröi ultraäänimoduulin
  pinMode (triggerPin, OUTPUT);
  
  //digital pin 3 input. Ultraääni moduuli lähettää echon tähän
  pinMode(echoPin, INPUT);

  pinMode(ledPin, OUTPUT);
}

int MeasureTemp(int door_open) {
  int temp = 0;
  
  Serial.print(" Requesting temperatures...");

  if (door_open == 0) {
  temp=sensors.getTempCByIndex(0); //ovi on juuri suljettu, mitataan lämpötila temp muuttujaan
  Serial.print("Temperature after door was closed: ");
  }
  else 
  {
  temp=sensors.getTempCByIndex(0); //jos ovi on auki, mitataan lämpötila temp2 muuttujaan
  Serial.print("Temperature when door was opened: ");
  }
  return(temp);
}
void MotionDetector () {
//etäisyysmittarin osuus
//mittaa etäisyyden sekunnin välein

  digitalWrite(triggerPin, LOW); //resetoidaan triggeröinti pin
  delayMicroseconds(2);
  digitalWrite(triggerPin, HIGH); //TriggerPin pulssi HIGH tilaan 10 microsekunniksi
  delayMicroseconds(10);
  digitalWrite(triggerPin, LOW); //Triggerpin pulssi LOW tilaan

  //Luetaan echoPin arvo, se palauttaa äänen liikkuman ajan mikrosekunneissa
  sensor_time = pulseIn(echoPin, HIGH);


  distance = sensor_time * 0.034 / 2;

  if (distance > 100 && door_open==0) //Jos etäisyys on yli 100 cm ja ovi on aikaisemmin ollut suljettuna
  {
    digitalWrite(ledPin, HIGH);
  door_open=1;
  temp = MeasureTemp(door_open); 
  Serial.println(temp);
  
  }
  else if (distance < 100 && door_open==1)//Jos distance on alle 100cm ja ovi on aikaisemmin ollut auki
  {
    digitalWrite(ledPin, LOW);
  door_open=0;
  temp = MeasureTemp(door_open);
  Serial.println(temp);
  }
}
void loop()
{
MotionDetector ();
digitalWrite(LED, digitalRead(LED) ^ 1); //xor
delay(1000);
}
