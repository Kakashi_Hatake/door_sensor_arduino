#include <OneWire.h>



const int triggerPin = 2; //ultraäänimoduulin triggeröinti
const int echoPin = 3; //ultraäänimoduulin echo
const int ledPin = 4; //ledin pinni

OneWire  ds(6);  // on pin 10 (a 4.7K resistor is necessary)

int distance;
long sensor_time;

int temp; //Viimeisin mitattu lämpötila
 
bool door_open; //onko ovi auki vai kiinni, 1 auki 0 kiinni

float temperature;
float newTemperature;

//lämpötilaosio alkaa
 

#define LED 12 //

void setup()
{
Serial.begin(9600);
pinMode(LED, OUTPUT);



  //digital pin 2 output, joka triggeröi ultraäänimoduulin
  pinMode (triggerPin, OUTPUT);
  
  //digital pin 3 input. Ultraääni moduuli lähettää echon tähän
  pinMode(echoPin, INPUT);

  pinMode(ledPin, OUTPUT);
}

int MeasureTemp(int door_open) {
  int temp = 0;
  
  Serial.print(" Requesting temperatures...");

  if (door_open == 0) {
  Serial.print("Temperature after door was closed: ");
  }
  else 
  {
  Serial.print("Temperature when door was opened: ");
  }
  return(temp);
}
void MotionDetector () {
//etäisyysmittarin osuus
//mittaa etäisyyden sekunnin välein

  digitalWrite(triggerPin, LOW); //resetoidaan triggeröinti pin
  delayMicroseconds(2);
  digitalWrite(triggerPin, HIGH); //TriggerPin pulssi HIGH tilaan 10 microsekunniksi
  delayMicroseconds(10);
  digitalWrite(triggerPin, LOW); //Triggerpin pulssi LOW tilaan

  //Luetaan echoPin arvo, se palauttaa äänen liikkuman ajan mikrosekunneissa
  sensor_time = pulseIn(echoPin, HIGH);


  distance = sensor_time * 0.034 / 2;

  if (distance > 100 && door_open==0) //Jos etäisyys on yli 100 cm ja ovi on aikaisemmin ollut suljettuna
  {
    digitalWrite(ledPin, HIGH);
  door_open=1;
  temp = MeasureTemp(door_open); 
  Serial.println(temp);
  
  }
  else if (distance < 100 && door_open==1)//Jos distance on alle 100cm ja ovi on aikaisemmin ollut auki
  {
    digitalWrite(ledPin, LOW);
  door_open=0;
  temp = MeasureTemp(door_open);
  Serial.println(temp);
  }
}

void TemperatureDetector (){
  getTemperature();
      //Serial.println(newTemperature);

  float tempMinus = temperature - newTemperature;
  if (tempMinus < 0){
    tempMinus *= -1;
  }

  if (tempMinus > 2){
    temperature = newTemperature;
    Serial.println("Temperature changed");
    Serial.println(temperature);
  }
  
}

void loop()
{
  TemperatureDetector();
MotionDetector ();
digitalWrite(LED, digitalRead(LED) ^ 1); //xor
delay(1000);
}

void getTemperature(){
  byte i;
  byte present = 0;
  byte type_s;
  byte data[12];
  byte addr[8];
  float celsius, fahrenheit;
    if ( !ds.search(addr)) {
    ds.reset_search();
    delay(250);
    return;
  }
    for( i = 0; i < 8; i++) {
  }
  ds.reset();
  ds.select(addr);
  ds.write(0x44, 1);        
  delay(1000);

  present = ds.reset();
  ds.select(addr);    
  ds.write(0xBE); 

  for ( i = 0; i < 9; i++) {
    data[i] = ds.read();
  }
  int16_t raw = (data[1] << 8) | data[0];
  if (type_s) {
    raw = raw << 3; 
    if (data[7] == 0x10) {
      
      raw = (raw & 0xFFF0) + 12 - data[6];
    }
  } else {
    byte cfg = (data[4] & 0x60);
    if (cfg == 0x00) raw = raw & ~7;
    else if (cfg == 0x20) raw = raw & ~3;
    else if (cfg == 0x40) raw = raw & ~1;
  }
  celsius = (float)raw / 16.0;
  fahrenheit = celsius * 1.8 + 32.0;

  newTemperature = celsius;
}
